## Sample Gitea Service

This Gitea service can also serve as a Git Resource in the local tier.

To deploy locally: `./g2 service deploy gitea giteaLocal1`

This will run a plain `kubectl apply -f` against the target env dir's `deploy` subdir. It isn't (yet) templatized for multiple instances.

The service solely provides template files for an env directory - it doesn't bundle generic deploy files (since it is all hard coded)

Handy dandy Gitea API via Java link: https://github.com/zeripath/java-gitea-api
